package model;

public class Student {
	private int id;
	private String name;

	public Student(int id) {
		this.id = id;
	}

	public void setName(String str) {
		name = str;
	}

	public String toString() {
		return id + "  " + name;
	}

}
